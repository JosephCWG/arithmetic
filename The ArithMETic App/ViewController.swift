//
//  ViewController.swift
//  The ArithMETic App
//
//  Created by Joseph on 2/7/19.
//  Copyright © 2019 Joseph Watts. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    // Picker view
    @IBOutlet weak var activityPickerView: UIPickerView!
    
    // Other UI Elements
    @IBOutlet weak var weightTextField: UITextField!
    @IBOutlet weak var exerciseTimeTextField: UITextField!
    @IBOutlet weak var energyConsumedLabel: UILabel!
    @IBOutlet weak var timeToLose1PoundLabel: UILabel!
    
    // Likely a better way to do this...
    let activities:[String] = Array(ExerciseCoach.sports.keys)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set up this class as the one containing the data and view.
        activityPickerView.delegate = self
        activityPickerView.dataSource = self
    }

    @IBAction func calculate(_ sender: Any) {
        // Convert what is in the text fields/uipickerview to vars
        let currentActivity = activities[activityPickerView.selectedRow(inComponent: 0)]
        let inputWeight = Double(weightTextField.text!)!
        let exerciseTime = Double(exerciseTimeTextField.text!)!
        
        // Set the energy consumed label
        energyConsumedLabel.text =
            String(format: "%.1f cal", ExerciseCoach.energyConsumed(during: currentActivity,
                                                weight: inputWeight,
                                                time: exerciseTime))
        // Set the time to loose 1 pound label
        timeToLose1PoundLabel.text =
            String(format: "%.1f minutes", ExerciseCoach.timeToLose1Pound(during: currentActivity,
                                                  weight: inputWeight))
    }
    
    @IBAction func clear(_ sender: Any) {
        // reset the pickerview
        activityPickerView.selectRow(0, inComponent: 0, animated: true)
        // reset all of the labels and text fields
        weightTextField.text = ""
        exerciseTimeTextField.text = ""
        energyConsumedLabel.text = "0 cal"
        timeToLose1PoundLabel.text = "0 minutes"
    }
    
    // Code related to the pickerview
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return activities.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return activities[row]
    }
}

struct ExerciseCoach {
    // All of the sports mapped to MET
    static let sports:[String:Double] = ["Bicycling": 8.0, "Jumping Rope": 12.3, "Running - Slow": 9.8, "Running - Fast": 23.0, "Tennis": 8.0, "Swimming": 5.8]
    
    // Static func to calculate the energy consumed by an activity over time with weight
    
    static func energyConsumed(during activity: String, weight: Double, time: Double) -> Double {
        let MET = ExerciseCoach.sports[activity]!
        // time * met * 3.5 * (weight in lb / lbs per kilo) / 200
        return time * MET * 3.5 * (weight/2.2) / 200
    }
    
    // Static func to calculate the time it takes to loose 1lb with a certain activity with weight
    static func timeToLose1Pound(during activity: String, weight: Double) -> Double {
        // 3500 calories makes up one pound. If we go backwards to calculate the calories in a minute
        // we can just divide 3500 by it to get the amount of time in minutes, to loose 1 lb.
        return 3500 / (ExerciseCoach.energyConsumed(during: activity, weight: weight, time: 1))
    }
}
